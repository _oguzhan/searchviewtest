package tr.com.mobilist.www.searchviewtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    String[] teams = new String[]{
            "Arizona Cardinals",
            "Chicago Bears",
            "Green Bay Packers",
            "New York Giants",
            "Detroit Lions",
            "Washington Redskins",
            "Philadelphia Eagles",
            "Pittsburgh Steelers",
            "Los Angeles Rams",
            "San Francisco 49ers",
            "Cleveland Browns",
            "Indianapolis Colts",
            "Dallas Cowboys",
            "Kansas City Chiefs",
            "San Diego Chargers",
            "Denver Broncos",
            "New York Jets",
            "New England Patriots",
            "Oakland Raiders",
            "Tennessee Titans",
            "Buffalo Bills",
            "Minnesota Vikings",
            "Atlanta Falcons",
            "Miami Dolphins",
            "New Orleans Saints",
            "Cincinnati Bengals",
            "Seattle Seahawks",
            "Tampa Bay Buccaneers",
            "Carolina Panthers",
            "Jacksonville Jaguars",
            "Baltimore Ravens",
            "Houston Texans"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        //searchView.setQueryHint("Arama Yap");
        ListView listView = (ListView) findViewById(R.id.listView);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, teams);
        if (listView != null) {
            listView.setAdapter(adapter);
        }

        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return false;
                }
            });
        }
    }
}
